const auth = require("./controller/auth");

class Routes {
  constructor(app) {
    this.app = app;
  }

  /* creating app Routes starts */
  appRoutes() {
    //Routes of authentication
    this.app.get("/auth", (req, res) => {
      const { user } = req;

      res.send({
        login: true,
        user: {
          _id: user._id,
          name: user.name,
          email: user.email,
          is_deleted: user.is_deleted,
        },
      });
    });
  }

  routesConfig() {
    this.appRoutes();
  }
}

module.exports = Routes;
