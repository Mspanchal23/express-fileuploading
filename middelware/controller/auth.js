const userModel = require("../../service/user/model/user.model");
// const adminModel = require("../../service/admin/model/admin.model");
const jwt = require("jsonwebtoken");

// this function authorization token
const auth = async function (authorization, next) {
  try {
    const userResult = await userModel.findUserByAuthToken(authorization);
    if (userResult.length > 0 && userResult != null && typeof userResult != undefined) {
      return next(null, userResult);
    }
    // const adminResult = await adminModel.findAdminByAuthToken(authorization);
    // if (adminResult.length > 0 && adminResult != null && typeof adminResult != undefined) {
    //   return next(null, userResult);
    // }

    const error = new Error("cant access");
    throw error.message;
  } catch (error) {
    next(error, null);
  }
};

const authStatic = async function (authorization, next) {
  try {
    // const userResult = await userModel.findUserByAuthToken(authorization);

    if (authorization != null && authorization == process.env.STATIC_TOKEN) {
      return next(null, true);
    } else {
      const error = new Error("cant access");
      throw error.message;
    }
  } catch (error) {
    next(error, null);
  }
};

// this function check unrestricted urls.
const checkUrl = async function (url) {
  const urls = ["/user_details/token", "/logout","/social_register","/social_login","/google_register_org"];

  if (typeof url !== "string") return false;
  return urls.includes(url);
};

// this function check customHeader
const customHeaderChecker = async (customHeader, next) => {
  const secret = process.env.CUSTOM_SECRET;
  const custom_header = process.env.CUSTOM_HEADER;
  jwt.verify(customHeader, secret, (err, decode) => {
    if (err) return next({ error: true }, { success: false });
    if (decode === custom_header) {
      next({ error: false }, { success: true });
    } else {
      next({ error: true }, { success: false });
    }
  });
};

module.exports = { auth, checkUrl, authStatic, customHeaderChecker };
