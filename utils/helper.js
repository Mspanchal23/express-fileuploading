
var sanitizer = require("../node_modules/sanitizer");
var Helper = function () {};

Helper.prototype.xss_clean = function (vals) {
    
    if(typeof vals == "object" && typeof vals.length != "undefined" && vals.length > 0){
        for(x in vals){
            vals[x] = sanitizer.sanitize(vals[x]);
        }
    }else if(typeof vals == "object" && typeof vals.length == "undefined" && Object.keys(vals).length > 0){
        let keys = Object.keys(vals)
        for(x in keys){
            vals[keys[x]] = sanitizer.sanitize(vals[keys[x]]);
        }
    }else{
        vals = sanitizer.sanitize(vals);
    }
    return vals
}
module.exports =new Helper();