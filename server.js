const express = require("express");
const helmet = require("helmet");
const http = require("http");
const bodyParser = require("body-parser");
const AppConfig = require("./config/app-config");
const UserRoutes = require("./service/user/route");
const EmployeeRoutes = require("./service/employee/router");
const StudentRoutes = require("./service/student/router");
const DepartmentRoutes = require("./service/student/dep.router");
const DepartRoutes = require("./service/Department/router")

// const AdminRoutes = require("./service/admin/route");
const logger = require("./config/winston");
const cors = require("cors");
const { auth, authStatic } = require("./middelware/controller/auth");

class Server {
  constructor() {
    this.app = express();
    this.app.use(cors());
    this.app.use(helmet());
    this.app.use(bodyParser.json({ limit: "50mb" }));
    this.app.use(bodyParser.urlencoded({ limit: "50mb" }));
    this.app.use(express.json({ limit: "50mb" }));
    this.app.use(express.urlencoded({ limit: "50mb" }));
    this.http = http.Server(this.app);

    
  }

  appConfig() {
    new AppConfig(this.app).includeConfig();
  }

  /* Including app Routes starts */
  includeRoutes() {
    new UserRoutes(this.app).routesConfig();
    
    new EmployeeRoutes(this.app).routesConfig();

    new StudentRoutes(this.app).routesConfig();

    new DepartmentRoutes(this.app).routesConfig();
    
    new DepartRoutes(this.app).routesConfig();
  }
  /* Including app Routes ends */

  startTheServer() {
    this.appConfig();
    this.includeRoutes();

    const port = process.env.NODE_SERVER_PORT || 4000;
    const host = process.env.NODE_SERVER_HOST || "localhost";

    this.http.listen(port, host, () => {
      logger.info(`Listening on http://${host}:${port}`);
    });
  }
}

module.exports = new Server();
