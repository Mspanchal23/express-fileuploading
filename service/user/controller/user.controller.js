const CONSTANTS = require("../../../config/constant");
const userModel = require("../model/user.model");
const util = require("../../../utils/response");
const message = require("../../../utils/messages.json");
const upload = require("../../../utils/upload");
const sanitizer = require("sanitizer");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const fs = require("fs");
const s3upload = require("../../../utils/upload");

class UserHandler {

  async createUser(request, response) {
    let reqData = request.body
    if (!reqData.name) {
      return response.status(400).send(util.error({}, message.customer_name_empty));
    } else if (!reqData.email) {
      return response.status(400).send(util.error({}, message.customer_email_empty));
    } else if (!reqData.mobile) {
      return response.status(400).send(util.error({}, message.customer_contact_empty));
    } else if (!reqData.password) {
      return response.status(400).send(util.error({}, message.customer_password_empty));
    } else if (!reqData.confirm_password) {
      return response.status(400).send(util.error({}, message.customer_confirm_psw_empty));
    } else {
      try {
        var resultEmail = await userModel.findUserByEmail(reqData.email);
        if (resultEmail && resultEmail.length > 0) {
          response.send(util.error({}, message.email_already_exist));
        } else {
          if (reqData.password == reqData.confirm_password) {
            let hash_pass = bcrypt.hashSync(reqData.password, 10);
            reqData.password = hash_pass;

            var users = await userModel.createUser(reqData);
            response.send(util.success(users, message.register_success));
          }else{
            response.send(util.success({}, message.psw_does_not_match));
          }
        }
      } catch (error) {
        response.send(util.error(error, message.common_messages_error));
      }
    }
  }

  async verifyEmailOTP(request, response) {
    let reqData = request.body;
    console.log(reqData, "VERIFUY SIDE");
    if (!reqData) {
      return response.status(400).send(util.error({}, message.User_otp_empty));
    } else {
      try {
        const CustExists = await userModel.findUserById(reqData.user_id);
        if (CustExists && CustExists.length > 0) {
          if (CustExists[0].otp == parseInt(reqData.otp)) {
            const payload = {
              id: CustExists[0]["_id"],
              email: CustExists[0]["email"],
            };
            const secret = process.env.JWT_SECRET;
            const token = jwt.sign(payload, secret);

            var updateUser = await userModel.UpdateUserInfoById(CustExists[0]._id, { verify_email: true, otp: null, token_id: token });
            var user_detail = await userModel.findUserById(CustExists[0]["_id"]);
            if (user_detail[0].org_id && user_detail[0].org_id != "") {
              var orgLogo = await userModel.findUserByIdForOrg(CustExists[0]["_id"]);
              user_detail.push({ org_logo: orgLogo[0].org_logo });
            }
            return response.status(200).send(util.success(user_detail, message.customer_otp_verified));
          } else {
            return response.status(200).send(util.error({}, message.customer_otp_not_exist));
          }
        } else {
          return response.status(200).send(util.error({}, message.customer_otp_not_exist));
        }
      } catch (err) {
        return response.status(400).send(util.error({}, message.something_went_wrong));
      }
    }
  }

  async login(request, response) {
    var reqData = {
      email: sanitizer.sanitize(request.body.email),
      password: sanitizer.sanitize(request.body.password),
    };
    try {
      var resultEmail = await userModel.findUserByEmail(reqData.email);

      if (resultEmail && resultEmail.length > 0) {
        bcrypt.compare(reqData.password, resultEmail[0].password, async function (err, pres) {
          if (pres == true) {
            const payload = {
              id: resultEmail[0]["_id"],
              email: resultEmail[0]["email"],
            };
            const secret = process.env.JWT_SECRET;
            const token = jwt.sign(payload, secret);
            await userModel.updateToken(resultEmail[0]["_id"], token);
            var user_detail = await userModel.findUserById(resultEmail[0]["_id"]);
            response.send(util.success(user_detail, message.login_success));
          } else {
            response.send(util.error([], message.in_correct_email_psw_error));
          }
        });
      } else {
        response.send(util.error([], message.email_does_not_already_exist));
      }
    } catch (error) {
      response.send(util.error(error, message.common_messages_error));
    }
  }


  async getUserById(request, response) {
    const { id } = request.params;
    try {
      const users = await userModel.findUserById(id);
      response.send(util.success(users, message.common_messages_record_available));
    } catch (error) {
      response.send(util.error(error, message.common_messages_error));
    }
  }

  async userEditByUserId(request, response) {
    var reqParam = request.params;
    var reqData = request.body;
    // console.log(reqData, "REQQ");
    if (reqParam.id) {
      try {
        // S3 Upload
        if (request.body.profile_pic && request.body.profile_pic != null) {
          var current_date = new Date();
          var timestamp = current_date.getTime();
          var fileName = "profile/" + timestamp + ".jpg";
          var decoded = new Buffer(request.body.profile_pic, "base64");
          var dbFilename = await s3upload.uploads3(fileName, decoded);
          reqData["profile_pic"] = dbFilename;
        }

        // Local Upload
        // if (request.body.profile_pic && request.body.profile_pic != null) {
        //   var current_date = new Date();
        //   var timestamp = current_date.getTime();
        //   var fileName = reqParam.id + ".jpg";
        //   var decoded = new Buffer(request.body.profile_pic, "base64");
        //   var dbFilename = await localUpload.uploadFile(decoded, fileName, "../upload/profile_pic/");
        //   console.log(dbFilename, "dbFilename");
        //   var dbFilename = await localUpload.getFile(fileName, "../upload/profile_pic/");
        //   reqData["profile_pic"] = dbFilename;
        // }
        const users = await userModel.UpdateUserInfoById(reqParam.id, reqData);
        const getuserByid = await userModel.findUserById(reqParam.id);
        if (users) {
          response.send(util.success(getuserByid, message.common_messages_profile_updated));
        } else {
          response.send(util.success([], message.common_messages_record_update_failed));
        }
      } catch (error) {
        response.send(util.error(error, message.common_messages_error));
      }
    } else {
      response.send(util.error({}, message.userId_missing));
    }
  }

  async verifyEmail(request, response) {
    let reqData = request.params.email;
    if (!reqData) {
      return response.status(400).send(util.error({}, message.email_empty));
    } else {
      try {
        const CustExists = await userModel.getUserByEmail(reqData);
        if (CustExists && CustExists.length > 0) {
          var otp = otpGenerator.generate(6, {
            digits: true,
            alphabets: false,
            upperCase: false,
            specialChars: false,
          });

          const CustEdit = await userModel.updateUserOtp(CustExists[0]._id, otp);
          if (CustEdit && CustEdit.nModified == 1) {
            var type = "forgot_password";
            let replace_arr = {};
            replace_arr["otp"] = otp;

            let user_arr = [];
            user_arr.push(CustExists[0].email);
            var to_name = CustExists[0].name;
            var mailResponse = await EmailModel.send(type, user_arr, replace_arr, to_name);
            if (mailResponse && mailResponse != null && mailResponse.messageId) {
              return response.status(200).send(util.success(message.mail_success));
            } else {
              response.status(200).send(util.error({}, message.email_already_exists));
            }
          } else {
            return response.status(400).send(util.error({}, message.common_messages_record_updated_failed));
          }
        } else {
          return response.status(400).send(util.error({}, message.email_doesnt_exists));
        }
      } catch (err) {
        return response.status(400).send(util.error({}, message.something_went_wrong));
      }
    }
  }

  async verifyOtp(request, response) {
    let reqData = request.params.otp;
    if (!reqData) {
      return response.status(400).send(util.error({}, message.User_otp_empty));
    } else {
      try {
        const CustExists = await userModel.getUserByOTP(reqData);
        if (CustExists && CustExists.length > 0) {
          return response.status(200).send(util.success(CustExists, message.customer_otp_verified));
        } else {
          return response.status(400).send(util.error({}, message.customer_otp_not_exist));
        }
      } catch (err) {
        return response.status(400).send(util.error({}, message.something_went_wrong));
      }
    }
  }

  async forgot_password(request, response) {
    let otp = request.params.otp;
    let reqData = request.body;
    if (!reqData.new_password) {
      return response.status(400).send(util.error({}, message.new_password_empty));
    } else if (!otp) {
      return response.status(400).send(util.error({}, message.customer_otp_empty));
    } else if (!reqData.cnew_password) {
      return response.status(400).send(util.error({}, message.confirm_password_empty));
    } else if (reqData.cnew_password != reqData.new_password) {
      return response.status(400).send(util.error({}, message.new_and_confirm_password_not_same));
    } else {
      try {
        let where = { forgot_otp: parseInt(otp), is_deleted: false };
        let obj = {
          password: bcrypt.hashSync(reqData.cnew_password, 6),
          forgot_otp: null,
        };
        const CustUpdate = await userModel.updateUserById(where, obj);
        if (CustUpdate && CustUpdate.nModified == 1) {
          return response.status(200).send(util.success(message.common_messages_record_updated));
        } else {
          return response.status(400).send(util.error({}, message.common_messages_record_updated_failed));
        }
      } catch (err) {
        return response.status(400).send(util.error({}, message.something_went_wrong));
      }
    }
  }

}
module.exports = new UserHandler();
