const userApi = require("./controller/user.controller");

class Routes {
  constructor(app) {
    this.app = app;
  }
  /* creating app Routes starts */
  appRoutes() {
    this.app.post("/register", userApi.createUser);
    this.app.post("/verify_email", userApi.verifyEmailOTP);
    this.app.post("/login", userApi.login);
    this.app.get("/user/:id", userApi.getUserById);


    this.app.put("/user_edit/:id", userApi.userEditByUserId);

    // Forgot Password
    this.app.post("/email/:email", userApi.verifyEmail);
    this.app.post("/otp/:otp", userApi.verifyOtp);
    this.app.put("/forgot/password/:otp", userApi.forgot_password);

  }

  routesConfig() {
    this.appRoutes();
  }
}
module.exports = Routes;
