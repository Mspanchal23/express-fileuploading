const mongoose = require("mongoose");
const mongooseTimestamp = require("mongoose-timestamp");
const CC = require("../../../config/constant_collection");
/**
 * Event Schema
 */
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  email: {
    type: String,
  },
  mobile: {
    type: String,
  },
  password: {
    type: String,
  },
  is_deleted: {
    type: Boolean,
    default: false,
  },
  token_id: {
    type: String,
  },

  status: {
    type: Boolean,
    default: true,
  },
  verify_email: {
    type: Boolean,
    default: false,
  },
  forgot_otp: {
    type: Number,
  },
  otp: {
    type: Number,
  },

});
UserSchema.plugin(mongooseTimestamp);
module.exports = mongoose.model(CC.U001_USERS, UserSchema);
