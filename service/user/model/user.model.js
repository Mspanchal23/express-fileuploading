"use strict";
const UserSchema = require("./user.schema");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const CC = require("../../../config/constant_collection")

class UserModel {
  constructor() {
    this.DB = require("../../../config/dbm");
    this.projectedKeys = {
      crtd_dt: true,
    };
  }

//  PREEEEEEE

  createUser(request) {
    let register_user = new UserSchema(request);

    return new Promise(async (resolve, reject) => {
      try {
        const result = await register_user.save();
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  findUserByEmail(email) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await UserSchema.find({
          email: email,
          // verify_email: true,
          is_deleted: false,
        });
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  findUserByStatus(id) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await UserSchema.find({
          _id: id,
          verify_email: true,
          is_deleted: false,
          status: true,
        });
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  findUserByEmailWithoutVerify(email) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await UserSchema.find({
          email: email,
          verify_email: false,
          is_deleted: false,
        });
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  findUserById(id) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await UserSchema.find({
          _id: ObjectId(id),
          is_deleted: false,
        });
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }


  findUserByAuthToken(token) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await UserSchema.find({
          token_id: token,
          is_deleted: false,
        });
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  updateToken(user_id, token) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await UserSchema.updateOne(
          { _id: ObjectId(user_id) },
          {
            $set: {
              token_id: token,
            },
          }
        );
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  deleteUserById(user_id, is_deleted) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await UserSchema.updateOne(
          { _id: ObjectId(user_id) },
          {
            $set: {
              is_deleted: is_deleted,
            },
          }
        );
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  getUserByEmail(email) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await UserSchema.find({ email: email, is_deleted: false });
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }


  UpdateStatusById(user_id, status) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await UserSchema.updateOne(
          { _id: ObjectId(user_id) },
          {
            $set: {
              status: status,
            },
          }
        );
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  UpdateUserInfoById(user_id, reqData) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await UserSchema.updateOne(
          { _id: ObjectId(user_id) },
          {
            $set: reqData,
          }
        );

        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }


  updateUserOtp(id, otp) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await UserSchema.updateOne({ _id: ObjectId(id) }, { $set: { forgot_otp: otp } });
        resolve(result);
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
  }

  getUserByOTP(otp) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await UserSchema.find({ forgot_otp: otp, is_deleted: false }).lean();
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  updateUserById(where, setObj) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await UserSchema.updateOne(where, { $set: setObj });
        resolve(result);
      } catch (error) {
        console.log(error);
        reject(error);
      }
    });
  }

  deleteUserById(id) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await UserSchema.updateOne({
          _id: ObjectId(id),
        },
          {
            $set: { is_deleted: true },
          });;
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

}

module.exports = new UserModel();
