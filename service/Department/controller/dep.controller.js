const CONSTANTS = require("../../../config/constant");
const depModel = require("../model/dep.model");
const util = require("../../../utils/response");
const message = require("../../../utils/messages.json");
const sanitizer = require("sanitizer");
const fs = require("fs");
const path = require("path");

class DepHandler {
  async createdepart(request, response) {
    // Validation logic
    let reqData = request.body;

    // console.log(reqData, "MSMSMSMSMSMSMS");
    if (!reqData.name && reqData.name == undefined) {
      return response.send("Please enter valid name");
    } else if (!reqData.age && reqData.age == undefined) {
      return response.send("Please enter valid age ");
    } else if (!reqData.gender && reqData.gender == undefined) {
      return response.send("Please enter valid email");
    }
    try {
      if (reqData) {
        // console.log(user, "");
        var user = await depModel.createUsere(reqData);
        console.log(file, "sssssssss");

        response.send(util.success(user, message.register_success));
      }
    } catch (error) {
      // console.log(user, "SSSSSS");
      response.send(util.error(error, message.common_messages_error));
    }
  }

  async profile(req, res) {
    console.log(res,'dddddd')
  
      let sampleFile;
      let uploadPath;

      if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send("No files were uploaded.");
      }
      sampleFile = req.files.photo;
      console.log(sampleFile, "CCAAAA");
      uploadPath = path.join(
        __dirname,
        "..",
        "..",
        "..",
        "photos",
        sampleFile.name
      );
      console.log(uploadPath,"ASDFGHJKL")

      sampleFile.mv(uploadPath, async function (err) {
        if(err) {
          return await res.send("err"); 
        
        }
        // console.log("File uploaded!");
        return await res.send("ampleFile");
      });
    
  }

  // async profile(req) {
  //   return new Promise((resolve, reject) => {
  //     try {
  //       if (!req.files || Object.keys(req.files).length === 0) {
  //         reject("No files were uploaded.");
  //       }

  //       const createFile = req.files.photo;
  //       console.log(createFile, "dddddddd");
  //       const uploadPath = path.join(
  //         __dirname,
  //         "..",
  //         "..",
  //         "..",
  //         "photos",
  //         createFile.name
  //       );
  //       console.log(__dirname, "aaaaaa");

  //       createFile.mv(uploadPath, function (err) {
  //         if (err) {
  //           reject(err);
  //         }

  //         resolve("File uploaded");
  //       });
  //     } catch (err) {
  //       reject(err);
  //     }
  //   });
  // }

  // async photo(request,response){
  //   console.log("object")
  //   response.json({message:'uploaded'})
  // }

  async getAlldeparts(request, response) {
    try {
      const deparment = await depModel.finddeparment();

      // async getAlldeparts(request, response) {
      //   try {
      //     const deparment = await depModel.finddeparment();
      // Count male and female records
      // let maleCount = 0;
      // let femaleCount = 0;

      // deparment.forEach((record) => {
      //   if (record.gender === 'male') {
      //     maleCount++;
      //   } else if (record.gender === 'female') {
      //     femaleCount++;
      //   }
      // });
      // const resultWithCounts = {
      //   data: deparment,
      //   counts: {
      //     total: deparment.length,
      //     male: maleCount,
      //     female: femaleCount
      //   },
      // };
      response.send(
        util.success(deparment, message.common_messages_record_available)
      );
    } catch (error) {
      response.send(util.error(error, message.common_messages_error));
    }
  }
}

module.exports = new DepHandler();
