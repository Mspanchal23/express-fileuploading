const mongoose = require("mongoose");
const mongooseTimestamp = require("mongoose-timestamp");
const CC = require("../../../config/constant_collection");

const userschema = new mongoose.Schema ({
    name:{
        type : String
    },
    age:{
        type: Number
    },
    gender:{
        type: String
    },
})
userschema.plugin(mongooseTimestamp);
module.exports = mongoose.model(CC.E001_USER,userschema);
