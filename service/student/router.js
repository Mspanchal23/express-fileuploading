const stuApi = require("./controller/stu.controller");

class Routes {
  constructor(app) {
    this.app = app;
  }
  appRoutes() {

// Create a new student
this.app.post('/student', stuApi.createstudent);

// get all student 
this.app.get('/students', stuApi.getAllstudents);

// Get one student by ID
this.app.get('/student/:id', stuApi.getstudentById);

// Update student by ID
this.app.put('/student/:id', stuApi.updatestudent);

// Delete student by ID
this.app.delete('/student/:id', stuApi.deletestudent);

}

routesConfig() {
  this.appRoutes();
}
}
module.exports = Routes;

