const CONSTANTS = require("../../../config/constant");
const stuModel = require("../model/stu.model");
const util = require("../../../utils/response");
const message = require("../../../utils/messages.json");
const sanitizer = require("sanitizer");

class StuHandler {
   
  // creater student 
  async createstudent(request, response) {
    // Validation logic
    let reqData = request.body
    console.log(reqData,"BODY")
  if (!reqData.stuname && reqData.stuname == undefined ) {
    return response.send("Please enter valid student-name")
  }
  else if (!reqData.department_id && reqData.department_id == undefined ) {
    return response.send("Please enter valid department-name ")
  }
  else if (!reqData.email && reqData.email == undefined ) {
    return response.send("Please enter valid email")
  }
  else if (!reqData.enrollnum && reqData.enrollnum == undefined ) {
    return response.send("Please enter valid enrollment-number")
  }
  else if (!reqData.sem && reqData.sem == undefined ) {
    return response.send("Please enter valid sem")
  }
    try { 
      if (reqData) {
        var student = await stuModel.createStudent(reqData);
        // console.log(student,"studentstudentstudentstudent")
        response.send(util.success(student, message.register_success));
      }
  } catch (error) {
    response.send(util.error(error, message.common_messages_error));
  }
}

// get all student by id
async getAllstudents(request, response) {
  try {
    const student = await stuModel.findStudent();
    response.send(util.success(student, message.common_messages_record_available));
  } catch (error) {
    response.send(util.error(error, message.common_messages_error));
  }
}

// get one student by id
async getstudentById(request, response) {



  
  const { id } = request.params;
  try {
    const student = await stuModel.findStudentById(id);
    response.send(util.success(student, message.common_messages_record_available));
  } catch (error) {
    response.send(util.error(error, message.common_messages_error));
  }
}

// update user id
async updatestudent(request, response) {
  var reqParam = request.params;
  var reqData = request.body;
  try { const student = await stuModel.UpdateStuInfoById(reqParam.id, reqData);
    const getEmpByid = await stuModel.findstudentById(reqParam.id);
    if (student) {
      response.send(util.success(getEmpByid, message.common_messages_profile_updated));
    } else {
      response.send(util.success([], message.common_messages_record_update_failed));
    }
  } catch (error) {
    response.send(util.error(error, message.common_messages_error));
  }
}
// delete by id
async deletestudent(request, response) {
  const { id } = request.params;
  try {
    const student = await stuModel.deleteStuloyeeById(id);
    response.send(util.success(student, message.common_messages_record_available));
  } catch (error) {
    response.send(util.error(error, message.common_messages_error));
  }
}

}

module.exports = new StuHandler();