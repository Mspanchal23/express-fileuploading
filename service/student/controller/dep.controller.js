const CONSTANTS = require("../../../config/constant");
// const stuModel = require("../model/stu.model");
const util = require("../../../utils/response");
const message = require("../../../utils/messages.json");
const sanitizer = require("sanitizer");
const depModel = require("../model/dep.model");

class DepHendler {
    async createdeparmente(request, response) {
        let reqData = request.body
        try { if (reqData) {
            var depart = await depModel.createDepart(reqData);
                    console.log(depart,"departr depart depart depart depart")
            response.send(util.success(depart, message.register_success));
          }
        } catch (error) {
        response.send(util.error(error, message.common_messages_error));
      }
    }

    // get all student by id
async getAlldeparments(request, response) {
  try {
    const deparment = await depModel.findDeparment();
    response.send(util.success(deparment, message.common_messages_record_available));
  } catch (error) {
    response.send(util.error(error, message.common_messages_error));
  }
}

}

module.exports = new DepHendler

