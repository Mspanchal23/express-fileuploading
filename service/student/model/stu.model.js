"use strict";
const stuSchema = require("./stu.schema");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const CC = require("../../../config/constant_collection");
// const {computer,it,comscince} = require("./stu.schema");

// create student
class studentMoodel {
  constructor() {
    this.DB = require("../../../config/dbm");
    this.projectedKeys = {
      crtd_dt: true,
    };
  }

  createStudent(request) {
    let register_student = new stuSchema(request);

    return new Promise(async (resolve, reject) => {
      try {
        // console.log('Innnnnnnn',register_student)
        const result = await register_student.save();

        //  console.log(result,"Resultss")
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  // find all students
  findStudent() {
    // return new Promise(async (resolve, reject) => {
    //   // console.log("_id", "SSSSS");
    //   try {
    //     const result = await stuSchema.aggregate([{
    //       $match: {
    //         is_deleted: false,
    //         is_active: true,
    //         // project_view_permission: true
    //       },
    //     },
    //     {
    //       $lookup: {
    //         from: CC.E001_DEP,
    //         localField: "department_id",
    //         foreignField: "_id",
    //         as: "department",
    //       }
    //     },
    //     {
    //       $unwind: {
    //         path: "$department",
    //         preserveNullAndEmptyArrays: true,
    //       },
    //     },
    //     {
    //       $project: {
            
    //         _id: 1,
    //           stuname: 1,
    //           email: 1,
    //           sem: 1,
    //           enrollnum:1,
    //           department_name: "$department.department",
           
    //       }
    //     }
    //     ]);
    //     resolve(result);
    //   } catch (error) {
    //     reject(error);
    //   }
    // });

    return new Promise(async (resolve, reject) => {
      try {
        const result = await stuSchema.find({});
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  // find student by id
  findStudentById(id) {
    return new Promise(async (resolve, reject) => {
      // console.log(id, "SSSSS");
      try {
        const result = await stuSchema.aggregate([
          {
            $match: { _id: mongoose.Types.ObjectId(id) },
          },
          {
            $lookup: {
              from: CC.E001_DEP,
              localField: "department_id",
              foreignField: "_id",
              as: "department",
            },
          },
          {
            $unwind: {
              path: "$department",
              preserveNullAndEmptyArrays: true,
            },
          },
          {
            $project: {
              _id: 1,
              stuname: 1,
              email: 1,
              sem: 1,
              enrollnum:1,
              // department_id:1,
              // dep

              // department_id: "$department_id",
              department_name: "$department.department",
            },
          },
        ]);
        console.log(result);
        resolve(result); // Assuming there's only one matching student
      } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
      }
    });
  }

  // update student by id
  UpdateStuInfoById(student_id, reqData) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await stuSchema.updateOne(
          { _id: ObjectId(student_id) },
          {
            $set: reqData,
          }
        );

        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  // delete student by id
  deleteStudentById(id) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await stuSchema.deleteOne(
          {
            _id: ObjectId(id),
          },
          {
            $set: { is_deleted: true },
          }
        );
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }
}

module.exports = new studentMoodel();
