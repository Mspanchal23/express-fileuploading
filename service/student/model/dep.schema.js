const mongoose = require("mongoose");
const mongooseTimestamp = require("mongoose-timestamp");
const CC = require("../../../config/constant_collection");

const depSchema = new mongoose.Schema({
    department : {
        type: String
    },
});

 depSchema.plugin(mongooseTimestamp);
 module.exports = mongoose.model(CC.E001_DEP, depSchema);
