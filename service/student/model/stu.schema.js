const mongoose = require("mongoose");
const mongooseTimestamp = require("mongoose-timestamp");
const CC = require("../../../config/constant_collection");
const ObjectId = mongoose.Types.ObjectId;


const stuSchema = new mongoose.Schema({
    stuname: {
        type :String
    },
    department_id: { 
        type : mongoose.Schema.Types.ObjectId,
        ref : CC.E001_DEP
    },
    email: {
        type :String
    },
    enrollnum: {
        type: Number
    },
    sem: {
        type :String
    }
    // Add other fields as needed age: Number
});

 stuSchema.plugin(mongooseTimestamp);
 module.exports = mongoose.model(CC.E001_STUDENT,stuSchema)
