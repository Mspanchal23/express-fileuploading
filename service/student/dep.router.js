const depApi = require("./controller/dep.controller");

class Routes {
    constructor(app) {
      this.app = app;
    }
    appRoutes() {
// Create a new student
this.app.post('/deparmente', depApi.createdeparmente);
// get all student 
this.app.get('/deparmentes', depApi.getAlldeparments);

    }

    routesConfig() {
      this.appRoutes();
    }
    }
    module.exports = Routes;