const employeeApi = require("./controller/emp.controller");

class Routes {
  constructor(app) {
    this.app = app;
  }
  appRoutes() {

// Create a new employee
this.app.post('/employee', employeeApi.createemployee);

// // get all employee 
this.app.get('/employees', employeeApi.getAllemployees);

// Get one employee by ID
this.app.get('/employee/:id', employeeApi.getemployeeById);

// Update employee by ID
this.app.put('/employee/:id', employeeApi.updateemployee);

// Delete employee by ID
this.app.delete('/employee/:id', employeeApi.deleteemployee);

}

routesConfig() {
  this.appRoutes();
}
}
module.exports = Routes;
