// const mongoose = require('mongoose');
// const Schema = mongoose.Schema;
const mongoose = require("mongoose");
const mongooseTimestamp = require("mongoose-timestamp");
const CC = require("../../../config/constant_collection");

 
const employeeSchema = new mongoose.Schema({
    username: {
        type :String
    },
    age: { 
        type :Number
    },
    email: {
        type :String
    },
    status: {
        type: String
    },
    phonum: {
        type :Number
    },
    experience : {
        type :Number
    }
    // Add other fields as needed age: Number
});

employeeSchema.plugin(mongooseTimestamp);
module.exports = mongoose.model(CC.E001_EMPLOYEES, employeeSchema);
