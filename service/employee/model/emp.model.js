"use strict";
const EmpSchema = require("./emp.schema");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const CC = require("../../../config/constant_collection")

// create employee 
class EmployeeModel {
    createEmployee(request) {
    let register_employee = new EmpSchema(request);

    return new Promise(async (resolve, reject) => {
      try {
        const result = await register_employee.save();
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  // find all employees 
  findEmployees() {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await EmpSchema.find({

        });
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

  // find employee by id 
  findEmployeeById(id) {
    return new Promise(async (resolve, reject) => {
      try {
        const result = await EmpSchema.find({
          _id: ObjectId(id),
          is_deleted: false,
        });
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

    // update employee by id 
  UpdateEmpInfoById(employee_id, reqData) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await EmpSchema.updateOne(
          { _id: ObjectId(employee_id) },
          {
            $set: reqData,
          }
        );

        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

    // delete employee by id 
  deleteEmployeeById(id) {
    return new Promise(async (resolve, reject) => {
      try {
        let result = await EmpSchema.deleteOne({
          _id: ObjectId(id),
        },
          {
            $set: { is_deleted: true },
          });;
        resolve(result);
      } catch (error) {
        reject(error);
      }
    });
  }

}

module.exports = new EmployeeModel();



