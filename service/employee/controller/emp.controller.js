const CONSTANTS = require("../../../config/constant");
const empModel = require("../model/emp.model");
const util = require("../../../utils/response");
const message = require("../../../utils/messages.json");
const sanitizer = require("sanitizer");
// const bcrypt = require("bcrypt");
// const jwt = require("jsonwebtoken");
// const fs = require("fs");
// const s3upload = require("../../../utils/upload");

class EmpHandler {
 
  // create employee 
  async createemployee(request, response) {
    // Validation logic
    let reqData = request.body
  if (!reqData.username && reqData.username == undefined ) {
    return response.send("Please enter valid username")
  }
  else if (!reqData.age && reqData.age == undefined ) {
    return response.send("Please enter valid age ")
  }
  else if (!reqData.email && reqData.email == undefined ) {
    return response.send("Please enter valid email")
  }
  else if (!reqData.status && reqData.status == undefined ) {
    return response.send("Please enter valid status")
  }
  else if (!reqData.phonum && reqData.phonum == undefined ) {
    return response.send("Please enter valid phonum")
  } 
  else if (!reqData.experience) {
    return response.send("Please enter valid experince")
  }
    try { 
      if (reqData) {
        var employee = await empModel.createEmployee(reqData);
        response.send(util.success(employee, message.register_success));
      }
  } catch (error) {
    response.send(util.error(error, message.common_messages_error));
  }
}

// get all employee by id
async getAllemployees(request, response) {
  try {
    const employee = await empModel.findEmployees();
    response.send(util.success(employee, message.common_messages_record_available));
  } catch (error) {
    response.send(util.error(error, message.common_messages_error));
  }
}

  // get one employee by id
  async getemployeeById(request, response) {
    const { id } = request.params;
    try {
      const employee = await empModel.findEmployeeById(id);
      response.send(util.success(employee, message.common_messages_record_available));
    } catch (error) {
      response.send(util.error(error, message.common_messages_error));
    }
  }

  // update user id
  async updateemployee(request, response) {
    var reqParam = request.params;
    var reqData = request.body;
    try { const employee = await empModel.UpdateEmpInfoById(reqParam.id, reqData);
      const getEmpByid = await empModel.findEmployeeById(reqParam.id);
      if (employee) {
        response.send(util.success(getEmpByid, message.common_messages_profile_updated));
      } else {
        response.send(util.success([], message.common_messages_record_update_failed));
      }
    } catch (error) {
      response.send(util.error(error, message.common_messages_error));
    }
  }
// delete by id
  async deleteemployee(request, response) {
    const { id } = request.params;
    try {
      const employee = await empModel.deleteEmployeeById(id);
      response.send(util.success(employee, message.common_messages_record_available));
    } catch (error) {
      response.send(util.error(error, message.common_messages_error));
    }
  }
}
module.exports = new EmpHandler();

